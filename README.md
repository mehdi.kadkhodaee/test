<img src="https://d33wubrfki0l68.cloudfront.net/f1f475a6fda1c2c4be4cac04033db5c3293032b4/513a4/assets/images/markdown-mark-white.svg" align="center" style="height: 20%"/>

                                  **This is my first ReadME file in Gitlab**

# Its my sample **Markdown language** file.

```mermaid
stateDiagram
    [*] --> Html
    Html --> [*]

   Body --> Moving
    Moving --> Body
    Moving --> Markdown
    Markdown --> [*]
```


 
### Header Text
# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6


### Links
  `[text](link)` sytnax:

[markdown](https://www.markdownguide.org/)


### Bold and Italic
This text **is bold**.  
This text *is italic*.  
This text ~~is struck out~~.
  

  
 

 

